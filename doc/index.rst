.. RHEAS documentation master file, created by
   sphinx-quickstart on Wed Jun 17 08:38:45 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to RHEAS's documentation!
=================================

The Regional Hydrologic Extremes Assessment System (RHEAS) is a hydrologic nowcast and forecast framework developed at the NASA Jet Propulsion Laboratory and the University of Massachuetts Amherst.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   docker
   api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
