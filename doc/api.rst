Extending RHEAS using the API
===============================

We can extend the functionality of RHEAS by using the API directly in custom scripts. Let's look at an example of calculating drought indices without having to re-run a simulation. We will assume that we have already ran a simulation between `2018-1-1` and `2019-12-31` and would like to calculate SPI, SRI and SMDI. We start by opening a Python shell or writing everything in a Python script and importing the necessary RHEAS modules

.. code-block:: python
		
		import numpy as np
		import tempfile
		from rheas import vic, drought, dbio

and then creating a `VIC` model instance that will help us run the drought calculation. Assuming that the resolution we've ran our model was 0.25 and the schema name we used was `basin` we can do

.. code-block:: python

		path = tempfile.mkdtemp(dir=".")
		model = vic.VIC(path, "rheas", 0.25, 2018, 1, 1, 2019, 12, 31, "basin")

Before we continue we also need to extract the latitude/longitude information from the database for our model (in a regular simulation that is done automatically)

.. code-block:: python
		
		db = dbio.connect(model.dbname)
		cur = db.cursor()
		cur.execute("select st_astext(geom) from {0}.basin".format(model.name))
		res = cur.fetchall()
		model.lon = [float(r[0][6:-1].split()[0]) for r in res]
		model.lat = [float(r[0][6:-1].split()[1]) for r in res]

The second pre-processing step we have to take (that would be handled automatically for us during a regular simulation) is the creation of the numpy grids that will hold the data

.. code-block:: python
		
		nrows = int(np.round((max(model.lat) - min(model.lat)) / model.res) + 1)
		ncols = int(np.round((max(model.lon) - min(model.lon)) / model.res) + 1)
		mask = np.zeros((nrows, ncols), dtype='bool')
		for c in range(len(model.lat)):
		    i = int((max(model.lat) + model.res / 2.0 - model.lat[c]) / model.res)
		    j = int((model.lon[c] - min(model.lon) + model.res / 2.0) / model.res)
		    mask[i, j] = True

Now let's choose the drought indices we want to calculate and write them to the database. The `writeToDB` function requires the data to be in an array with shape `(nt, 1, nrows, ncols)` where `nt` is the number of time steps

.. code-block:: python
		
		ensemble = False
		nt = (date(model.endyear, model.endmonth, model.endday) -
                  date(model.startyear + model.skipyear, model.startmonth, model.startday)).days + 1
		mi, mj = np.where(mask)
		for var in ["spi3", "sri6", "smdi"]:
		    data = np.zeros((nt, 1, nrows, ncols))
		    data[:, 0, mi, mj] = drought.calc(var, model, ensemble)
		    model.writeToDB(data, None, var, ensemble)

In this example we had a deterministic simulation, but in case of a stochastic simulation we can unset `ensemble` and wrap it in a loop that will go over the ensemble size. 
		    
		    
