Running RHEAS with Docker
============================

`Docker <https://www.docker.com>`_ is a virtualization platform that can deliver software in containerized packages (i.e., containing all dependencies within its environment). We provide a Docker solution for running RHEAS, by either building your own images or downloading them from DockerHub.

If you have already cloned the RHEAS repository, just change to that directory where `docker-compose.yml` exists or just download it directly.

After you have that file, you can start the services (basically the PostGIS database) using

.. code-block:: bash

   docker-compose up -d

which exposes the database to host software (e.g. QGIS). The first time you run this command, Docker will download the images if they don't exist on your system and the initialize your database so give it a few minutes. The Docker stack will create a volume where the database contents will reside, so you will have permanent storage for your data. If the volume is not created, we would lose all the data every time the Docker container would stop.

After that you can run RHEAS using

.. code-block:: bash

   docker-compose run rheas bin/rheas

and supply the arguments as usual. By default, the rheas container will mount
the current working directory as `/data`, so if you had a file `nocwast.conf`
and you wanted to pass it as an argument you would do

.. code-block:: bash

   docker-compose run rheas rheas /data/nowcast.conf

.. warning::

   Also note that any paths you have in your configuration files need to be relative to the `/data` mount point.

Let's set an alias to make the command easier to call

.. code-block:: bash

   alias rheas='docker-compose run rheas rheas'

and then you can use it as

.. code-block:: bash

   rheas /data/nowcast.conf

After your run is finished you can examine the created database tables either
using a GIS software or via the command line

.. code-block:: bash

   psql -h localhost -U rheas -W -d rheas

after typing `rheas` at the prompt as your password.

You can also run a RHEAS-enabled Python shell

.. code-block:: bash

   docker-compose run rheas python3

Within that Python shell, you can either test out the RHEAS API or do things like add cultivar information for your agricultural simulations

.. code-block:: python

      from rheas.dssat import utils as dutils
      def create_rice_cultivar(p1, p2o, p2r, p5, g1, g2, g3, g4):
          return {'p1': p1, 'p2o': p2o, 'p2r': p2r, 'p5': p5, 'g1': g1, 'g2': g2, 'g3': g3, 'g4': g4}
      jasmine84 = create_rice_cultivar(330, 11.4, 312, 807, 71.8, 0.0338, 0.86, 1.1)
      jasmine84['name'] = "'jasmine84'"
      vd20 = create_rice_cultivar(186, 305, 843, 12.3, 71.8, 0.0365, 0.95, 1.09)
      vd20['name'] = "'vd20'"
      params = [jasmine84, vd20]
      dutils.addCultivar("rheas", "../adpc/THA_Adm1_GISTA_plyg_v5.shp", params, crop="rice")

To stop the Docker services (mostly the database) you can run

.. code-block:: bash

   docker-compose stop

Building your own images
----------------------------

There might be a situation when you would like to customize your own images, you can either extend the existing RHEAS images or directly edit the included Dockerfiles. After you have your specification, you can build the images

.. code-block:: bash

    cd RHEAS
    docker build -t rheas-db -f rheasdb.dockerfile .
    docker build -t rheas -f rheas.dockerfile .
